package fft;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileIO {
  static String DEFAULTDIR;
  static String OUTPUTFILE;
  static String OUTPUTFILEBASE;
  static String WAVHEADER;
  static String COMPFILE;
  
  public static void setPaths()
  {
    if (System.getProperty("user.name").equals("nmacias")){
// Nick
      DEFAULTDIR="c:/users/nmacias/files/FFTData";
      OUTPUTFILE="c:/users/nmacias/Desktop/output.wav";
      OUTPUTFILEBASE="c:/users/nmacias/Desktop/output";
      WAVHEADER="c:/users/nmacias/files/FFTData/header.wav";
      COMPFILE="c:/users/nmacias/files/FFTData/compare";
      System.out.println("Hey Nick");
    } else {
// Jordan
      DEFAULTDIR="/home/leusid/Homework/fft";
      OUTPUTFILE="/home/leusid/Homework/fft/output.wav";
      OUTPUTFILEBASE="/home/leusid/Homework/fft/output";
      WAVHEADER="/home/leusid/Homework/fft/header.wav";
      COMPFILE="/home/leusid/Homework/fft/compare";
      System.out.println("Hey Jordan");
    }
  }


    static public DataOutputStream writeHeader(String fname)
    {
      DataOutputStream os;
      FileInputStream is;
    byte[] buf=new byte[4];
    long size;

    try {
      is=new FileInputStream(FileIO.WAVHEADER);
      os = new DataOutputStream(new FileOutputStream(fname));
    } catch (FileNotFoundException e1) {
      e1.printStackTrace();
      return(null);
    } 
    
    // write header for wave file
    for (int i=0;i<40;i++){
      int c=0;
      try {
        c = is.read(); // read one char
        if (i==24) // store LSB of sample rate
          c=44100 & 0xff;
        if (i==25)
          c=(44100 >> 8) & 0xff;
        
        buf[0]=(byte) (c&0xff);
        os.write(buf[0]);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } 
    }
    
    size=44100*4*20; // # bytes in 20 seconds;
    try {
      buf[0]=(byte)(size&0xff);
      buf[1]=(byte)((size>>8)&0xff);
      buf[2]=(byte)((size>>16)&0xff);
      buf[3]=(byte)((size>>24)&0xff);
      os.write(buf[0]);os.write(buf[1]);os.write(buf[2]);os.write(buf[3]);
      is.close(); // done with header file
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return(os);
    }  

    static public double[] readWaveData(FileInputStream fis, double time)
  {
    double length=44100*time;
    byte[] buffer=new byte[4];

    double[] waveData=new double[1+(int) length];
    int outIndex=0;

    for (outIndex=0;outIndex<length;outIndex++){
      try {
        if (-1 == fis.read(buffer)){ // EOF
          fis.close();
          return(null);
        }
      } catch (IOException e) {
        e.printStackTrace();
        System.exit(0); // do we ever hit this???
      } // read 4 bytes
      double value=buffer[0]+(buffer[1]*256); // create y coordinate
          if (value > 32767) value=value-65536;
          value=value/32767; // [-1:1]
      waveData[outIndex]=value;
    } // sample data read

    return(waveData);
  }
    static public void writeData(DataOutputStream os,Pattern p)
    {
        double time,x;
        int value;
        double limit=p.timeDuration;
        byte[] buffer = new byte[2];
        for (time=0;time<limit;time+=(1./44100.))
        {
              x = Pattern.reverseFFT(p,time);
              value = (int)(32767.*x); // write this value
              if (value < 0) value=65536+value; // 2's comp!
              buffer[0] = (byte) (value & 0xff); // low byte
              buffer[1] = (byte) ((value>>8) & 0xff); // next byte
              try {
                os.write(buffer[0]); // left channel
                os.write(buffer[1]); // left channel
                os.write(buffer[0]); // right channel
                os.write(buffer[1]); // right channel
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        return;
      } 
        }
    }
    
    static public void writeSpacer(DataOutputStream os)
    {
      double timeLimit=1.; // 1 second spacer
        byte[] buffer = new byte[2];

      for (double time=0;time<timeLimit;time+=(1./44100)){
        double y=32767*Math.sin(2.*Math.PI*200.*time);
            int value = (int) y;
            if (value < 0) value=65536+value;
            if ((time < .4) || (time > .6)) value=0; // silence
            buffer[0] = (byte) (value & 0xff); // low byte
            buffer[1] = (byte) ((value>>8) & 0xff); // next byte
            try {
              os.write(buffer[0]); // left channel
                os.write(buffer[1]); // left channel
                os.write(buffer[0]); // right channel
                os.write(buffer[1]); // right channel
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        return;
      }
      } 
        
    }

  static public FileInputStream openWaveData(String fName)
  {
    FileInputStream fis=null;  // main input stream
    byte[] header=new byte[44];
    
    try {
      fis=new FileInputStream(fName);
      try {
        fis.read(header);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      System.out.println("Error: cannot open " + fName + ": " + e);
      return (null);
    }
    return(fis);
  }
  
  // write data to output file (simple test)
  public void writePattern(DataOutputStream dos, Pattern p)
  {
    FileIO.writeSpacer(dos);
    FileIO.writeData(dos,p); // save length inside pattern, use that in writeData
  }
}
