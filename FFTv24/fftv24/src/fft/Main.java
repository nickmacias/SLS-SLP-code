package fft;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main extends JFrame {
  private static final long serialVersionUID = 1424544959914332760L;
  boolean click=false,release=false,drag=false;
  int lastX=0,lastY=0; // last mouse press coordinates
  MainThread theThread=null;
  double[] newData;
  int dragY=0; // 0=flag, 99=high amp

  public DataOutputStream dos; // this is where things get written to;

  private JPanel contentPane;
  static Main frame;

  static JFileChooser chooser; // common file dialog

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          frame = new Main();
          frame.setVisible(true);
          frame.setSize(900,700);

          FileIO.setPaths(); // check username, set I/O paths

          chooser = new JFileChooser(FileIO.DEFAULTDIR);
            FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Wave file", "wav");
            chooser.setFileFilter(filter);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  public Main() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 300);

    JMenuBar menuBar = new JMenuBar();
    setJMenuBar(menuBar);

    JMenu mnFile = new JMenu("File");
    mnFile.setMnemonic(KeyEvent.VK_F);
    menuBar.add(mnFile);

    JMenuItem mntmOpenWaveFile = new JMenuItem("Open Wave File");
    mntmOpenWaveFile.setMnemonic(KeyEvent.VK_O);
    mntmOpenWaveFile.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
          int returnVal = chooser.showOpenDialog(contentPane);
          if(returnVal == JFileChooser.APPROVE_OPTION) {
// this is the main action...
            newData=new double[Pattern.NUMFREQ]; // scratchpad
            theThread=new MainThread(frame,chooser.getSelectedFile().getAbsolutePath());
            theThread.start();
          }
      }
    });
    mnFile.add(mntmOpenWaveFile);

    JMenuItem mntmOutputStuff = new JMenuItem("Output Stuff");
    mntmOutputStuff.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
      }
    });
    mntmOutputStuff.setMnemonic(KeyEvent.VK_S);
    mnFile.add(mntmOutputStuff);

    JMenuItem mntmSaveState = new JMenuItem("Save State");
    mntmSaveState.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    	try{
    	// SAVE STATE HERE
		  FileOutputStream fos = new FileOutputStream("state");
	      ObjectOutputStream oos = new ObjectOutputStream(fos);
	      oos.writeObject(theThread.allPatterns);
	      oos.close();
    	} catch(Exception e){
    	  System.out.println("Some sort of error (" + e + ") while saving state");
    	}
      }
    });
    mnFile.add(mntmSaveState);

    JMenuItem mntmRestoreState = new JMenuItem("Restore State");
    mntmSaveState.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    	try{
    	// RESTORE STATE HERE
		  FileInputStream fis = new FileInputStream("state");
	      ObjectInputStream ois = new ObjectInputStream(fis);
	      theThread.allPatterns=(Pattern[]) ois.readObject();
	      ois.close();
    	} catch(Exception e){
    	  System.out.println("Some sort of error (" + e + ") while reading state");
    	}
    	}
    });
    mnFile.add(mntmRestoreState);

    JMenu mnExit = new JMenu("Exit");
    mnExit.setMnemonic(KeyEvent.VK_X);
    menuBar.add(mnExit);

    JMenuItem mntmExitImmediately = new JMenuItem("Exit Immediately");
    mntmExitImmediately.setMnemonic(KeyEvent.VK_X);
    mntmExitImmediately.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent arg0) {
        System.exit(0);
      }
    });
    mnExit.add(mntmExitImmediately);
    contentPane = new JPanel();
    contentPane.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent arg0) {
        click=true;release=false;
        lastX=arg0.getX();lastY=arg0.getY();
        repaint();
      }
      @Override
      public void mouseReleased(MouseEvent arg0) {
        click=false;release=true;
        repaint();
      }
    });
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
  }

  Pattern lastPat=null;
  int lastIndex=0;

  public void savePat(Pattern p, int index)
  {
    lastPat=p;
    lastIndex=index;
  }

// Display FFTs
  boolean init=true;
  public void paint(Graphics g)
  {
    if (init) super.paint(g);
    init=false;

    if (lastPat==null) return;
    localDraw(g,lastIndex); // paint the spectrum
  }

  public void superRepaint()
  {
    super.repaint();
  }

  void localDraw(Graphics g,int index)
  {
    int row,col;
    double[] data;

    row=(index/5)+1;
    col=(index % 5);

  // show the spectrum
    g.clearRect(100*col,100*row,100,100);
    g.drawRect(100*col,100*row,100,100);
    double sum=0;

    if (click){ // pressed mouse: display weighted spectrum
      click=false;
      int lastRow=((lastY+45)/100)-1;
      int lastCol=(lastX/100);
      int lastIndex=5*lastRow + lastCol;
      ++lastRow;
      if ((lastIndex < 0) || (lastIndex > 24)) return; // ooops!

      g.clearRect(100*lastCol,100*lastRow,100,100);
      g.drawRect(100*lastCol,100*lastRow,100,100);
      data=theThread.allPatterns[lastIndex].WS;
      //for (int i=0;i<Pattern.NUMFREQ;i++) System.out.println(i + ":" + data[i]);

      for (int x=0;x<Pattern.NUMFREQ;x++){
        sum+=data[x]; // tally all frequencies in this band
        if (((x+1)%30)==0){ // paint!
            sum=sum/30.; // average
            sum=sum*100; // scale to window
          if (sum > 100) sum=100;
          //System.out.println("sum=" + sum);
          g.fillRect(100*lastCol+3*(x/30),100+100*lastRow-(int)(sum),2,(int)(sum));
          sum=0;
        }
      }
    } else if (release){ // release mouse: show normal spectrum (coeff[])
      release=false;
      int lastRow=((lastY+45)/100)-1;
      int lastCol=(lastX/100);
      int lastIndex=5*lastRow + lastCol;
      ++lastRow;
      if ((lastIndex < 0) || (lastIndex > 24)) return; // ooops!

      g.clearRect(100*lastCol,100*lastRow,100,100);
      g.drawRect(100*lastCol,100*lastRow,100,100);

      data=theThread.allPatterns[lastIndex].coeff;
      for (int x=0;x<Pattern.NUMFREQ;x++){
        sum+=data[x];
        if (((x+1)%30)==0){ // paint!
            sum=sum/30.; if (sum > 1) sum=1;
          g.fillRect(100*lastCol+3*(x/30),100+100*lastRow-(int)(100*sum),2,(int)(100*sum));
          sum=0;
        }
      }
    } else { // plot raw data based on last call to savePat()
      data=lastPat.coeff;
      for (int x=0;x<Pattern.NUMFREQ;x++){
        sum+=data[x];
        if (((x+1)%30)==0){ // paint!
            sum=sum/30.;if (sum > 1) sum=1;
          g.fillRect(100*col+3*(x/30),100+100*row-(int)(100*sum),2,(int)(100*sum));
          sum=0;
        }
      }
    }

  }


  void lllocalDraw(Graphics g,int index)
  {
    int row,col;
    double[] data;

    row=(index/5)+1;
    col=(index % 5);

  // show the spectrum
    //g.clearRect(100*col,100*row,100,100);
    g.drawRect(100*col,100*row,100,100);
    double sum=0;
    if (click || drag){ // pressed mouse: normalize etc.
      click=drag=false;
      int lastRow=((lastY+45)/100)-1;
      int lastCol=(lastX/100);
      int lastIndex=5*lastRow + lastCol;
      ++lastRow;
      if ((lastIndex < 0) || (lastIndex > 24)) return; // ooops!

      g.clearRect(100*lastCol,100*lastRow,100,100);
      g.drawRect(100*lastCol,100*lastRow,100,100);
      //System.out.println("Last [r,c]=" + lastRow + "," + lastCol + "  index=" + lastIndex);
      data=theThread.allPatterns[lastIndex].coeff;
      for (int x=0;x<Pattern.NUMFREQ;x++) newData[x]=data[x];

      // calculate area
      double tArea=0;
      for (int x=0;x<Pattern.NUMFREQ;x++){
        tArea+=newData[x];
      }
      double aSum=0;int xx;
      for (xx=0;xx<Pattern.NUMFREQ;xx++){
        aSum+=newData[xx];
        if (aSum > .05*tArea) break;
      } // x is 5% point
      for (int i=xx;i<Pattern.NUMFREQ;i++){
        newData[i-xx]=data[i]; // shift down
      }
      for (int i=Pattern.NUMFREQ-xx;i<Pattern.NUMFREQ;i++){
        newData[i]=0; // clear the tail
      }

      // scale so total area=1
      aSum=0;
      for (int x=0;x<Pattern.NUMFREQ;x++) aSum+=newData[x]; // total area
      if (aSum==0) return;
      for (int x=0;x<Pattern.NUMFREQ;x++){
        newData[x]=20*dragY*newData[x]/aSum;
      }

      for (int x=0;x<Pattern.NUMFREQ;x++){
        sum+=newData[x];
        if (((x+1)%30)==0){ // paint!
          if (sum > 100) sum=100;
          g.fillRect(100*lastCol+3*(x/30),100+100*lastRow-(int)(sum),2,(int)(sum));
          sum=0;
        }
      }
    } else if (release){
      release=false;
      int lastRow=((lastY+45)/100)-1;
      int lastCol=(lastX/100);
      int lastIndex=5*lastRow + lastCol;
      ++lastRow;
      if ((lastIndex < 0) || (lastIndex > 24)) return; // ooops!

      g.clearRect(100*lastCol,100*lastRow,100,100);
      g.drawRect(100*lastCol,100*lastRow,100,100);

      data=theThread.allPatterns[lastIndex].coeff;
      for (int x=0;x<Pattern.NUMFREQ;x++){
        sum+=data[x];
        if (((x+1)%30)==0){ // paint!
          g.fillRect(100*lastCol+3*(x/30),100+100*lastRow-(int)(100*sum),2,(int)(100*sum));
          sum=0;
        }
      }
    } else { // plot raw data
      data=lastPat.coeff;
      for (int x=0;x<Pattern.NUMFREQ;x++){
        sum+=data[x];
        if (((x+1)%30)==0){ // paint!
          g.fillRect(100*col+3*(x/30),100+100*row-(int)(100*sum),2,(int)(100*sum));
          sum=0;
        }
      }
    }
  }
}
