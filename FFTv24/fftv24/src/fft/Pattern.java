
package fft;

import java.io.Serializable;
/* future comments:
 *
 *
 *  use C as inverse of REL: REL=0, C=1; REL=inf, C=0
 *  so we lock in patterns more as we see them more often
 *  DON'T adjust C based on difference between patterns...
 *  adjust it based only on the relevance of the pattern in the buffer.
 *
 *
 *
        |                                   ***********
        |                                ***
        |                             ***
        |                       *
        |                  *
        |              *
        |           *
        |         *
        |       *
        |      *
        |     *
        |     *
        |    *
        |    *
        |   *
        |   *
        |  *
        |  *
        | *
        | *
        |*
        +--------------------------------------------------





        |*
        | *
        | *
        |  *
        |  *
        |   *
        |   *
        |    *
        |    *
        |     *
        |     *
        |      *
        |       *
        |         *
        |           *
        |              *
        |                  *
        |                       *
        |                             ***
        |                                ***
        |                                   ***********
        +--------------------------------------------------





        |                                                 *
        |                                              *
        |                                           *
        |                                        *
        |                                     *
        |                                  *
        |                               *
        |                            *
        |                         *
        |                      *
        |                   *
        |                *
        |             *
        |          *
        |       *
        |    *
        |  *
        |*
        +--------------------------------------------------


 *
 */

class Pattern implements Serializable{
  private static final long serialVersionUID = -8404679018796198211L;
  public static final int NUMFREQ=1000;
  public static final double OMEGA0=16;

  double[] coeff=new double[NUMFREQ]; // coeffs of combined sin/cos series
  double[] a=new double[NUMFREQ];  // coeffs of sine series
  double[] b=new double[NUMFREQ]; // coeffs of cosine series
  double [] WS=new double[NUMFREQ]; // weighted spectrum
  //double[] area=new double[NUMFREQ]; // integral from 0 to n
  double timeDuration=0;
  double relevance; // how often have we seen this pattern?
  double DIFFSCALING=20.; // scale difference by this, so any diff>=.05 is absolutely different
  double BASE_RELEVANCE=11.;
  double SIM_THRESHOLD=.5;//.9;
  double staleness=0.;
  double c;

// method to compare this pattern to an array of patterns,
// and update each pattern's relevance and spectrum
  //public void compare(Pattern[] allPatterns)
  //{
  //  for (int i=0;i<allPatterns.length;i++){
  //    compare(allPatterns[i]);
  //  }
  //}

// compare ourself to an incoming pattern (p)
// First compute sqrt(sum(squares(area diffs))). That tells how similar two patterns are
// Set our C input based on the similarity and our relevance;
// then adjust our own pattern based on the incoming pattern.
public double compare(Pattern p)
  {
    double aDiff,sim;
    double result=0; // set of we reset a pattern buffer's patern

    aDiff=diff(p); // find difference in weighted spectrums
    aDiff=aDiff*DIFFSCALING;
    if (aDiff > 1) aDiff=1; // really need things to be similar to start considering them
    sim=1-aDiff;
    if (sim > SIM_THRESHOLD){ // close match 0 morph the pattern with this new one

      c=sim/((relevance/BASE_RELEVANCE)*2.);
    // adjust pattern based on C

      //???staleness=0; // fresh pattern!
      result=sim;
    } else {
      ++staleness;
      if (staleness > relevance*relevance){ // re-seed the pattern
        result=-1;
      }
    } // DONE

    //c=Math.pow(sim,relevance); // this shows how much to change our pattern
    //c=Math.pow(Math.E, -relevance); //
    ////c=Math.pow(sim,2); // this shows how much to change our pattern

// new C calc: 0-.5 increase; .5-1 decrease
    //if (sim > .5) c=1-2*(sim-.5);
    //else c=2*sim;
    //c=c/Math.E; // tame it down...

    System.out.print("sim=" + sim + "; c=" + c + "; relevance=" + relevance);
    //double r=Math.pow(sim,1./relevance);
    //relevance=relevance+sim*relevance;
    //System.out.println("; new relevance=" + relevance);
    // since coeff[] has changed, re-compute the weighted spectrum...
    //calcWeightedSpectrum();

    return(result);
  }

  void morph(Pattern p)
  {
      for (int i=0;i<Pattern.NUMFREQ;i++){
          a[i]=p.a[i]*c + a[i]*(1-c);
          b[i]=p.b[i]*c + b[i]*(1-c);
          coeff[i]=p.coeff[i]*c + coeff[i]*(1-c);
        }
      relevance=relevance+1; // harder to discard
      staleness=0;
  }

  void reseed(Pattern p)
  {
      for (int i=0;i<Pattern.NUMFREQ;i++){
          a[i]=p.a[i];
          b[i]=p.b[i];
          coeff[i]=p.coeff[i];
        }
      relevance=BASE_RELEVANCE;
      staleness=0;
  }


  void calcWeightedSpectrum4()  // no adjustments!
  {
    for (int i=0;i<Pattern.NUMFREQ;i++){
        WS[i]=coeff[i];
    }
  }

  void calcWeightedSpectrum()  // sum of area from 0 to f
  {
  // for the weighted spectrum, calculate the total area from 0 to f
  // normalize so at the highest frequency, the area is 1.
    double area=0;
    for (int i=0;i<Pattern.NUMFREQ;i++){
        area+=coeff[i];
        WS[i]=area;
    }

    // "area" is now the total area
    for (int i=0;i<Pattern.NUMFREQ;i++){
      WS[i]=WS[i]/area;
    }
    return;
  }

    void calcWeightedSpectrum2()
    {
      double max=0;
    // at each frequency, convolve with e^(-x^2);
      for (int i=0;i<Pattern.NUMFREQ;i++){
        WS[i]=convolve(i);
        if (WS[i]>max) max=WS[i];
      }
      if (max==0) return;
      for (int i=0;i<Pattern.NUMFREQ;i++){
        WS[i]=WS[i]/max;
      }
    }

    double convolve(double center)
    {
      double sum=0;
      // scale e^(-x^2) so that the high/low points are e
      // do this by multiplying (center-i) by 2*e/NUMFREQ
      double scale=5.*Math.E/Pattern.NUMFREQ; // 2* should be reasonable?...

      for (double i=0;i<Pattern.NUMFREQ;i++){
          int ind=(int) i;
          double exp=(center-i)*scale;
          exp=-exp*exp;
        sum+=coeff[ind]*Math.exp(exp)*scale; // 1100 ne 249th st ridgefield
      } // this sums the frequency spectrum's components, scaling the center frequency by 1 and
        // sideband frequencies by smaller values, according to an e^(-x^2) profile.
      sum=sum/(Math.sqrt(Math.PI)); // area under profile curve
      return(sum);
    }

    public double diff(Pattern in)
    {
        int i;
        double diff=0;
        double scale; // 1 for low side, 0 for high side (where all areas = 1)

        for(i = 0; i < NUMFREQ; i++)
        {
          scale=((double)i)/((double)NUMFREQ);  // 0->1
          scale=1.-scale;  // 1->0
            if (WS[i]+in.WS[i] != 0){
              diff+=(Math.abs(WS[i] - in.WS[i])/(WS[i]+in.WS[i]))*(scale);
            }
        }
        diff=diff/NUMFREQ; // average diff
        diff=2*diff; // since scaling by a shape with total area=1
        return (diff);
    }

  Pattern(double[] a,double[] b, double length)
  {
    this.a=a;this.b=b;this.timeDuration=length;
    relevance=BASE_RELEVANCE;
    staleness=0.; // lots of comments here
  }

  static Pattern FFT(double[] rawData) // do the time-to-frequency conversion. len is sample length (sec)
  {
    double value,dt,t;

    double a,b;
    double[] bcoef=new double[Pattern.NUMFREQ];
    double[] acoef=new double[Pattern.NUMFREQ];

    double length = rawData.length;

      for (double n = 0; n < Pattern.NUMFREQ; n++){
      double freq=Pattern.OMEGA0*n;
        a=b=0;
        dt=1./44100.;t=0;
        for(int index=0;index<rawData.length;index++){
          value=rawData[index]; // next sample
            a+=value*Math.cos(freq*t)*dt;
            b+=value*Math.sin(freq*t)*dt;
          t=t+dt;
        }

        a=a*2./t;b=b*2./t;
        if (n==0){a=a/2;b=0;}
          acoef[(int)n]=a;
          bcoef[(int)n]=b;
      }
      Pattern ret=new Pattern(acoef,bcoef,length/44100.);
      ret.normalize(); // populate the v[] and (normalized) area arrays :)
      ret.calcWeightedSpectrum();         // also calculate the weighted spectrum
      return(ret);
  }

  static public double reverseFFT(Pattern p, double t)
    {
        double s;
        int n;
        s=p.a[0]/2.;
        for (n=1;n<Pattern.NUMFREQ;n++)
        {
            double freq=Pattern.OMEGA0*(double)n;
            s+=p.a[n]*Math.cos(freq*t) + p.b[n]*Math.sin(freq*t);
        }
        return(s/10.); // for linear
    }

  void normalize()  // one-time calculation of combined coefficients and total area
  {
    //double ar=0; // running sum of area

    //normalize(); // scale components so largest = 1

    for (int i=0;i<NUMFREQ;i++){  // combined coefficients
      coeff[i]=Math.sqrt((a[i]*a[i] + b[i]*b[i])/2.);
    }

    /***
    // normalize: calculate area
    double tArea=0;
    for (int x=0;x<Pattern.NUMFREQ;x++){
      tArea+=coeff[x];
    }
    double aSum=0;int xx;
    for (xx=0;xx<Pattern.NUMFREQ;xx++){
      aSum+=coeff[xx];
      if (aSum > .05*tArea) break;
    } // x is 5% point
    for (int i=xx;i<Pattern.NUMFREQ;i++){
      coeff[i-xx]=coeff[i]; // shift down
      a[i-xx]=a[i];
      b[i-xx]=b[i];
    }
    for (int i=Pattern.NUMFREQ-xx;i<Pattern.NUMFREQ;i++){
      coeff[i]=0; // clear the tail
      a[i]=0;
      b[i]=0;
    }
    ***/

/***
    // scale so max component=1
      double max=0;
      for (int x=0;x<Pattern.NUMFREQ;x++) if (coeff[x] > max) max=coeff[x];
      if (max==0) return;
      for (int x=0;x<Pattern.NUMFREQ;x++){
        coeff[x]=coeff[x]/max;
        a[x]=a[x]/max;
        b[x]=b[x]/max;
      }
***/
    }


  public void print()
  {
    double max=0;int maxind=0;
    double area=0;
    for (int i=0;i<coeff.length;i++){
      area+=coeff[i];
      System.out.format("%.2f:%5f   ",Pattern.OMEGA0*(double)i,coeff[i]);
      if (coeff[i] > max){
        max=coeff[i];
        maxind=i;
      }
    }
    System.out.println("\n Max value=" + max + ": maxfreq=" + maxind*(int)OMEGA0+"  Area=" + area);
  }

  public void plot()
  {
    for (int i=20;i>0;i--){ // height
      for (int j=0;j<NUMFREQ;j++){ // frequency
            System.out.print(((coeff[j]*19.5-.5) > i)?'*':' ');
      }
      System.out.println("");
    }

    for (int j=0;j<NUMFREQ;j++) System.out.print("-");
    System.out.println("");
  }
}
