/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ContactEditorUI.java
 *
 * Created on Nov 16, 2011, 6:46:20 PM
 */
package my.analogio;
import java.net.*;
import java.io.*;
import javax.swing.*;
import java.util.LinkedList;


/**
 *
 * @author nick
 */

/*
 * We communicate through either a serial port or a socket with an analog IO device
 * using a series of simple commands.
 * The analog IO device is considered the server, and this engine is the client.
 * 
 * ENGINE -> ANALOG IO DEVICE:
 * 32-96     output value of (n-32) on current channel
 * 97-102    set analog device's current output channel to n-97 
 * 124 + 32  clear all monitoring of analog inputs
 * 124 + n   monitor analog input #(n-33)
 * 125       re-send all analog input values to client
 * 
 * ANALOG IO DEVICE -> ENGINE:
 * 32-96     analog input value of (n-32) on current channel
 * 97-102    set engine's current input channel to n-97
 * 126       Attention signal from analog IO device: exit (or reset) the engine
 *
 */

public class AnalogIOUI extends javax.swing.JFrame {
    Sockthread st=null; // thread that handles all socket IO
    static Socket sock; // main socket for communication with CM engine
    static InputStream is; // this is where we communicate
    static OutputStream os;

    /** Creates new form ContactEditorUI */
    public AnalogIOUI() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    int currentInputChannel=(-1); // what channel are we receiving from engine?
    int mon[]={0,0,0,0,0,0}; // which input channels are we monitoring?
    int currentOutputChannel=(-1); // what channel are we *sending* to the engine?

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        input1 = new javax.swing.JSlider();
        input2 = new javax.swing.JSlider();
        input3 = new javax.swing.JSlider();
        input0 = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        meter0 = new javax.swing.JProgressBar();
        meter1 = new javax.swing.JProgressBar();
        meter2 = new javax.swing.JProgressBar();
        meter3 = new javax.swing.JProgressBar();
        jLabel6 = new javax.swing.JLabel();
        ChanVal1 = new javax.swing.JLabel();
        ChanVal0 = new javax.swing.JLabel();
        ChanVal2 = new javax.swing.JLabel();
        ChanVal3 = new javax.swing.JLabel();
        exitButton = new javax.swing.JButton();
        attnButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CM3G Analog Interface");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel3.setToolTipText("");
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        input1.setForeground(new java.awt.Color(241, 6, 41));
        input1.setMajorTickSpacing(200);
        input1.setMaximum(1000);
        input1.setMinorTickSpacing(50);
        input1.setOrientation(javax.swing.JSlider.VERTICAL);
        input1.setPaintTicks(true);
        input1.setToolTipText("");
        input1.setValue(0);
        input1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        input1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                input1StateChanged(evt);
            }
        });
        jPanel3.add(input1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, -1, -1));

        input2.setForeground(new java.awt.Color(241, 6, 41));
        input2.setMajorTickSpacing(200);
        input2.setMaximum(1000);
        input2.setMinorTickSpacing(50);
        input2.setOrientation(javax.swing.JSlider.VERTICAL);
        input2.setPaintTicks(true);
        input2.setToolTipText("");
        input2.setValue(0);
        input2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        input2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                input2StateChanged(evt);
            }
        });
        jPanel3.add(input2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, -1, -1));

        input3.setForeground(new java.awt.Color(241, 6, 41));
        input3.setMajorTickSpacing(200);
        input3.setMaximum(1000);
        input3.setMinorTickSpacing(50);
        input3.setOrientation(javax.swing.JSlider.VERTICAL);
        input3.setPaintTicks(true);
        input3.setToolTipText("");
        input3.setValue(0);
        input3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        input3.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                input3StateChanged(evt);
            }
        });
        jPanel3.add(input3, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, -1, -1));

        input0.setForeground(new java.awt.Color(241, 6, 41));
        input0.setMajorTickSpacing(200);
        input0.setMaximum(1000);
        input0.setMinorTickSpacing(50);
        input0.setOrientation(javax.swing.JSlider.VERTICAL);
        input0.setPaintTicks(true);
        input0.setToolTipText("");
        input0.setValue(0);
        input0.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        input0.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                input0StateChanged(evt);
            }
        });
        jPanel3.add(input0, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        jLabel1.setText("In 0");
        jPanel3.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 270, -1, -1));

        jLabel2.setText("In 1");
        jPanel3.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 270, -1, -1));

        jLabel3.setText("In 2");
        jPanel3.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 270, -1, -1));

        jLabel4.setText("In 3");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 270, -1, -1));

        jLabel5.setText("Analog Inputs");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, -1, -1));

        meter0.setFont(new java.awt.Font("Courier 10 Pitch", 0, 13)); // NOI18N
        meter0.setBorder(javax.swing.BorderFactory.createTitledBorder("Channel 0"));
        meter0.setString("");
        meter0.setStringPainted(true);

        meter1.setFont(new java.awt.Font("Courier 10 Pitch", 0, 13));
        meter1.setBorder(javax.swing.BorderFactory.createTitledBorder("Channel 1"));
        meter1.setString("");
        meter1.setStringPainted(true);

        meter2.setFont(new java.awt.Font("Courier 10 Pitch", 0, 13));
        meter2.setBorder(javax.swing.BorderFactory.createTitledBorder("Channel 2"));
        meter2.setString("");
        meter2.setStringPainted(true);

        meter3.setFont(new java.awt.Font("Courier 10 Pitch", 0, 13));
        meter3.setBorder(javax.swing.BorderFactory.createTitledBorder("Channel 3"));
        meter3.setString("");
        meter3.setStringPainted(true);

        jLabel6.setText("Analog Outputs");

        ChanVal1.setText("0.0 V");
        ChanVal1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        ChanVal0.setText("0.0 V");
        ChanVal0.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        ChanVal2.setText("0.0 V");
        ChanVal2.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        ChanVal3.setText("0.0 V");
        ChanVal3.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        exitButton.setForeground(new java.awt.Color(245, 6, 6));
        exitButton.setText("EXIT");
        exitButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        exitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitButtonActionPerformed(evt);
            }
        });

        attnButton.setForeground(new java.awt.Color(227, 12, 80));
        attnButton.setText("Attention");
        attnButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        attnButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attnButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jLabel6)
                .addContainerGap(158, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(meter2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(meter3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(meter0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(meter1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(attnButton)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(exitButton)
                        .addContainerGap(79, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ChanVal1, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                            .addComponent(ChanVal0, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                            .addComponent(ChanVal2, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                            .addComponent(ChanVal3, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE))
                        .addGap(59, 59, 59))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(exitButton)
                    .addComponent(attnButton))
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(ChanVal0)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ChanVal1)
                        .addGap(44, 44, 44)
                        .addComponent(ChanVal2)
                        .addGap(44, 44, 44)
                        .addComponent(ChanVal3))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(meter0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(meter1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(meter2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(meter3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(37, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // we need a Runnable class for handling updates to the GUI
    // since all such updates should be run from a single (EventHandler) thread
    
    class meterUpdate implements Runnable
      {
        LinkedList<Integer> updateQ=new LinkedList(); // indicate queue will only contain ints :)
        
        // invoke saveCV() prior to putting object on event queue
        // values are pure (between 0 and 63)
        // Note this may be called more than once before it actually runs
        // on the eventqueue; so we need to save each call, and pop off
        // all updates later
       
        // But actually...we don't necessarily want to show *all* the updates
        // for a channel; it may suffice to only show the latest *per channel*
       
        int chanVal[]={-1,-1,-1,-1}; // -1 means no update necessary
       
        void saveCV(int channel, int value){
            //updateQ.add(channel);
            //updateQ.add(value);
            chanVal[channel]=value; // save latest value
        }

        // record for what channel we're receiving engine output
        void setCurrentInputChannel(int chan)
        {
            currentInputChannel=chan;
        }
        
        // run() method will be executed via the invokeLater() method
        @Override public void run()
        {
            Object obj1=null,obj2=null;
            int saveChannel,saveValue; // pull these from the updateQ

            for (int i=0;i<4;i++){ // see which channels are updated
              if (chanVal[i] != -1){ // update this channel
                saveValue=chanVal[i];
                switch(i){
                    case 0:obj1=meter0;obj2=ChanVal0;break;
                    case 1:obj1=meter1;obj2=ChanVal1;break;
                    case 2:obj1=meter2;obj2=ChanVal2;break;
                    case 3:obj1=meter3;obj2=ChanVal3;break;
                    default: return; // Ooops???
                }
                ((JProgressBar)obj1).setValue((saveValue*100)/63);
                ((JLabel)obj2).setText(round((((double)saveValue)*5./63.),3.) + "V");
                chanVal[i]=(-1); // record that we've already updated this
              } // end of update for channel i
            }
            
            /***
            while (!updateQ.isEmpty()){
                saveChannel=updateQ.remove();
                saveValue=updateQ.remove();                
                switch(saveChannel){
                    case 0:obj1=meter0;obj2=ChanVal0;break;
                    case 1:obj1=meter1;obj2=ChanVal1;break;
                    case 2:obj1=meter2;obj2=ChanVal2;break;
                    case 3:obj1=meter3;obj2=ChanVal3;break;
                    default: return; // Ooops???
                }
                ((JProgressBar)obj1).setValue(saveValue);
                ((JLabel)obj2).setText(round((((double)saveValue)*5./63.),3.) + "V");
            } // get next pair from updateQ
            ***/
          }
        
        double round(double in, double num)
        {
            return( ( (int) (in*(Math.pow(10.,num))))/Math.pow(10.,num));
        }
      }
    
    class Sockthread extends Thread{
       ServerSocket ss=null;
       
       @Override public void run(){
                int b; // readback byte
                boolean doMore=true; // keep going
                int portNum=1221;
                
        // make a meterUpdate object for updting the display
        meterUpdate MU=new meterUpdate();
        
        // open a socket and wait for a (single) connection
        try{
            ss=new ServerSocket(portNum);
        } catch (Exception e){
            System.out.println("Error creating server-side socket (port " + portNum + "): " + e);
            System.exit(1);
        }
        
        // main loop - accept connctions and process messages lol as opposed to?
        while (doMore){
           try{
                sock=ss.accept();
                System.out.println("Connection received");

                is=sock.getInputStream();
                os=sock.getOutputStream();
            } catch (Exception e){
                System.out.println("Error in accept() or stream association: " + e);
                System.exit(1);
            }            

            // slider changes will send through os; so we can sit here and
            // wait for input (through is) and use it to update the meter displays
            try{
                while (-1 != (b=is.read())){
                   // parse this character
                    if ((b >= 32) && (b <= 96)){ // normal input on current channel
                        MU.saveCV(currentInputChannel,b-32);
                    // now call invokeLater() to post changes to the displayed meters
                      SwingUtilities.invokeLater(MU);
                    } else if ((b >= 97) && (b <= 100)){ // change the channel
                        currentInputChannel=b-97;
                    } else if (b==123){ // SYNC request - just respond with 123
                        os.write(123);
                        System.out.println("Sync\n");
                    } else if (b==124){ // receive request for monitoring analog inputs
                        b=is.read();
                        if (b==32) mon[0]=mon[1]=mon[2]=mon[3]=mon[4]=mon[5]=0; // clear all monitors
                        else if ((b >= 33) && (b <= 38)) mon[b-33]=1;
                    } else if (b==125){ // re-send all (monitored) analog channels
                        if (mon[0]==1){os.write(97);os.write(32+(input0.getValue()*63/1000));}
                        if (mon[1]==1){os.write(98);os.write(32+(input1.getValue()*63/1000));}
                        if (mon[2]==1){os.write(99);os.write(32+(input2.getValue()*63/1000));}
                        if (mon[3]==1){os.write(100);os.write(32+(input3.getValue()*63/1000));}
                        //if (mon[4]==1){os.write(96);os.write(32+input4.getValue());}
                        //if (mon[5]==1){os.write(96);os.write(32+input5.getValue());}                    
                    } // else we don't know what to do with this character
                } // loop here forever
            } catch (Exception e){ // client probably closed connection
                System.out.println(e + ": client closed connection?");
            }
        }
      }
      
      void shutDown()
      {
          try{
            is.close();
            os.close();
            System.exit(0);
          } catch (Exception e){
              System.out.println("Unexpected error closing socket; " + e);
          }
      }
      
    }  // end of Sockthread
    
    private void input0StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_input0StateChanged
      inputChanged(0,input0.getValue());
    }//GEN-LAST:event_input0StateChanged

    private void input1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_input1StateChanged
        inputChanged(1,input1.getValue());
    }//GEN-LAST:event_input1StateChanged

    private void input2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_input2StateChanged
        inputChanged(2,input2.getValue());
    }//GEN-LAST:event_input2StateChanged

    private void input3StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_input3StateChanged
        inputChanged(3,input3.getValue());
    }//GEN-LAST:event_input3StateChanged

    // generic routine for handling an input change
    void inputChanged(int channel,int value)
    {
      //msgbox("Channel " + channel + "=" + value);
      if (mon[channel] == 0) return; // engine isn't interested in this channel
      try{
          if (channel != currentOutputChannel){ // change channels
              os.write(channel+97);
          } // now continue with data send
          os.write(((value*63)/1000)+32);
      } catch (Exception e){
          System.out.println("Error writing to server: " + e);
      }
    }
    
    void msgbox(String s)
    {
       // MsgBox.setText(MsgBox.getText() + "\n" + s);
    }

    // is this the right place to instantiate this???
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        //msgbox("Waiting for connection from engine");
        st=new Sockthread();
        st.start(); // wait for connection, then receive inputs and send to display
    }//GEN-LAST:event_formWindowOpened

    private void attnButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_attnButtonActionPerformed
    {//GEN-HEADEREND:event_attnButtonActionPerformed
        try{
            os.write(126);
        } catch(Exception e){
            System.out.println("Error sending Attn to socket: " + e);
        }
    }//GEN-LAST:event_attnButtonActionPerformed

    private void exitButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_exitButtonActionPerformed
    {//GEN-HEADEREND:event_exitButtonActionPerformed
        st.shutDown();
        System.exit(0);
    }//GEN-LAST:event_exitButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
   
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override public void run() {
                new AnalogIOUI().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ChanVal0;
    private javax.swing.JLabel ChanVal1;
    private javax.swing.JLabel ChanVal2;
    private javax.swing.JLabel ChanVal3;
    private javax.swing.JButton attnButton;
    private javax.swing.JButton exitButton;
    private javax.swing.JSlider input0;
    private javax.swing.JSlider input1;
    private javax.swing.JSlider input2;
    private javax.swing.JSlider input3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JProgressBar meter0;
    private javax.swing.JProgressBar meter1;
    private javax.swing.JProgressBar meter2;
    private javax.swing.JProgressBar meter3;
    // End of variables declaration//GEN-END:variables
}
