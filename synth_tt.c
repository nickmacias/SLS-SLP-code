#include "engine.h"
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <malloc.h>

// MATRIX DIMS
#define ROWS 8
#define COLS 8

main(int argc,char **argv)
{
  int lval,rval,kval; // these are used by the SET_TT() macro
  int i,j,k;
  double x,y,z;

  if (argc != 2){
    fprintf(stderr,"Usage: %s outputfile\n",argv[0]);
    return(1);
  }

  if (init_CM(ROWS,COLS)) return(1); // failure - out of memory?
  if (init_output(ROWS,COLS,argv[1])) return(1); // couldn't create file

// setup TTs based on desired function

  for (lval=0;lval<64;lval++){
    for (rval=0;rval<64;rval++){
      for (kval=0;kval<64;kval++){
        #include ".synth_tt_include"
      }
    }
  }

// now save the CM array to a file for quick retrieval later`
  write_output();
  close_output(); // and exit neatly
}

static int tt_fp=(-1);

init_output(int rows, int cols, char *name)
{
  char buffer[120];

  tt_fp=open(name,O_CREAT|O_RDWR|O_TRUNC,0666);
  if (tt_fp <= (long int)NULL){fprintf(stderr,"Error: can't open %s for writing\n",name);return(1);}

// write the header
  sprintf(buffer,"V3G.noc.1.0\n2\n%d\n%d\n",rows,cols);
  write(tt_fp,buffer,strlen(buffer));
  return(0);
}

// now write the CM array into a file
write_output()
{
  int row,col,index;
  char buffer[120];
  char *MyCM;

  MyCM=fetch_CM();
  for (row=0;row<ROWS;row++){
    for (col=0;col<COLS;col++){
     // index=(64*64*64)*4*(row*COLS + col); // index into CM array
      index=(col << 21) | (row << 24);
      printf("%c                   %cWriting [%d,%d]",13,13,row,col);fflush(stdout);
      write(tt_fp,&MyCM[index],64*64*64*8); // 64^3 input combos; 4 sidex*C/D=8 columns
    }
  }
  write(tt_fp,"EOF\n",4);
  printf("%cDone                             \n",13);
}

int close_output()
{
  close(tt_fp);
}
