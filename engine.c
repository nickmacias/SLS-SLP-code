/********
o When computing D output, combine with C input/TT bits
o track current bit# of TT read/write
o Periodically (or when queue is empty?) insert a TICK into the queue
o when a TICK is popped, update TT bits and current bit# (all cells)
 ( forget about "dual modes"...modality falls on a continuous spectrum now)

SPECIFIC C-MODE PLAN

ALWAYS (when setting outputs in the usual way following input changes; or
        when tick -> 0):
Each Dout=(TT's Dout)*(63-Cin)/63 + (TT[bitnum])*Cin/63 (per side)
Each Cout=(TT's Cout)*(63-Cin)/63 (per side)

WHEN TICK -> 1:
Latch Cin: Clatch=max(all Cin)
Latch Din as follows:
  Dlatch=(DL*CL + DR*CR + DK*CK)/(CL+CR+CK)
   (if all C=0, just latch a 0...it won't be used anyway)

WHEN TICK -> 0:
 - store latched bit:
     TT[bitnum]=TT[bitnum]*(63-Cin)/63 + Dlatch*Cin/63
 - increment bitnum
 - set Dout and Cout as above

********/

#define DEBUG 0
#include "engine.h"

/*
 * main structures for 3rd gen Cell Matrix: continuous TTs
 *   :)
 */

/*
 * We communicate through either a serial port or a socket
 * with an analog IO device using a series of simple commands.
 * The analog IO device is considered the server, and this engine is the client.
 *
 * ENGINE -> ANALOG IO DEVICE:
 * 32-96     output value of (n-32) on current channel
 * 97-102    set analog device's current output channel to n-97
 * 124 + 32  clear all monitoring of analog inputs
 * 124 + n   monitor analog input #(n-33)
 * 125       re-send all analog input values to client
 * 123       SYNC: Reply with 123 when you receive this
 *
 * ANALOG IO DEVICE -> ENGINE:
 * 32-96     analog input value of (n-32) on current channel
 * 97-102    set engine's current input channel to n-97
 * 126       Attention signal from analog IO device: exit (or reset) the engine
 * 123       SYNC response
 *
 */

// make these variables so we can pass them in the instantiator
#define QSIZE 100000 // # entries

// 6-bit inputs/outputs (64 value approximation to continuous interval)

// S1S0=(00:K 01:R 10:L)
// C.S1S0=(000:DK  001:DR  010=DL  100=CK  101=CR  110=CL)

// TT: L R K | CL CR CK DL DR DK
// At address L5L4L3L2L1L0 R5R4R3R2R1R0 K5K4K3K2K1K0 S1S0 we store the
// 6 (8) output bits b5b4b3b2b1b0 for that side S1S0 DATA
// Store all 3 sides for D, then all 3 for C

// CM ARRAY:
// At address r2r1r0 c2c1c0 L5L4L3L2L1L0 R5R4R3R2R1R0 K5K4K3K2K1K0 S1S0 we store the
// 6 (8) output bits b5b4b3b2b1b0 for that side C.S1S0 in cell [r,c]
// So the full address is:
//   r2r1r0 c2c1c0 L5L4L3L2L1L0 R5R4R3R2R1R0 K5K4K3K2K1K0 C.S1.S0

// QUEUE STRUCTURE:
// Each entry looks like:
// r2r1r0 c2c1c0 C.S1S0 V5V4V3V2V1V0
// which indicates a new *input* value V for side (C/D) S in cell [r,c]

// INPUT ARRAY: stores edge-input values
// r2r1r0 c2c1c0 C.S1S0 stores v5v4v3v2v1v0 (edge input)

// ADJACENCY:
// [r,c]R:[r,c+1]L
// [r,c]L:[r,c-1]R
// [r,c]K: either [r+1,c]K (r even) or
//                [r-1,c]K (r odd)

#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

// evil globals
int inputs[512]; // no room for right/bottom edges booooo
// 64 cells*4 sides*2(C/D)

int queue[QSIZE]; // 14-bit entries
int qhead=0; // next READ loc
int qtail=0; // next WRITE loc
// Empty if qhead=qtail

int timeflag=0; // toggle 0<-->1 when clock ticks
long events=0; // # of events processed
long realtime=0; // elapsed time (prop delays)

int bitnum=0; // Current bit being read/written in C-mode
int bitnum_iter=0; // counts # of full C-mode cycles
int bitnum_next; // temporary
int dlatch[8][8], clatch[8][8];

int sync_wait=0; // set when we've sent a SYNC pulse and still need a reply

int ROWS=8,COLS=8; // THE dimensions (default 8x8)
// NOTE than the TT ingest occurs after I/O are setup, but we don't know
// the engine dims until after the TT load. This needs to be fixed.
// NJM 19 Nov 2011

// arrays that show mapping from abstract channel # to [row,col].side
// inputs FROM analog interface TO CM engine
int IROWS[6],ICOLS[6],ISIDES[6];
int IVALID[6]={0,0,0,0,0,0}; // set to 1 if channel info is meaningful

// outputs FROM CM engine TO analog interface
int OROWS[6],OCOLS[6],OSIDES[6];
int OVALID[6]={0,0,0,0,0,0};
int OLAST[6]={-1,-1,-1,-1,-1,-1};

// this is the SD card actually...
// (or not)
char *CM;
int fp; // serial port or socket-based channel
int INPUT_EVENTS=0; // # of actual input changes

int get_time()
{
  return(realtime);
}

int get_events()
{
  return(events);
}

// load structures showing mapping from abstract channel# to [row,col].side
setup_input_channel(row,col,side,channel)
{
  if ((channel < 0) || (channel > 6)){
    fprintf(stderr,"Ooops: Bad channel number %d\n",channel);
    return;
  }
  IROWS[channel]=row;
  ICOLS[channel]=col;
  ISIDES[channel]=side;
  IVALID[channel]=1;

// tell analog interface to monitor this channel
  send_byte(124);send_byte(33+channel);
}

setup_output_channel(row,col,sideIn,channel)
{
  int side;

  if ((channel < 0) || (channel > 5)){
    fprintf(stderr,"Ooops: Bad channel number %d\n",channel);
    return;
  }

  side=sideIn&3; // ignore C/D value for now

// we actually store the corresponding *input* so set_input() can detect the edge output
  if ((col==0) && (side == L)){
    col=(-1);
    side=R;
  } else if ((col==COLS-1) && (side==R)){
    col=COLS;
    side=L;
  } else if (row==0){
    if (side != K){
      fprintf(stderr,"Can't access L/R outputs from row 0 or %d\n",ROWS-1);
      return;
    }
    if (((row+col)&1) == 0){
      fprintf(stderr,"Cannot access cell [%d,%d]'s K output\n",row,col);
      return;
    }
    row=(-1);
    //side=K;
  } else if (row==ROWS-1){
    if (side != K){
      fprintf(stderr,"Can't access L/R output from row 0 or %d\n",row);
      return;
    }
    if (((row+col)&1) == 1){
      fprintf(stderr,"Cannot access cell [%d,%d]'s K output\n",row,col);
      return;
    }
    row=ROWS;
    //side=K;
  } else {
    fprintf(stderr,"ERROR: [%d,%d]%d is not accessible for output monitoring\n",
           row,col,side);
    return;
  }
  if (OVALID[channel]){
    fprintf(stderr,"WARNING: channel %d is already in use\n",channel);
    return;
  }
// All set here :)
  OROWS[channel]=row;
  OCOLS[channel]=col;
  OSIDES[channel]=side + (sideIn&4); // use original side to get C/D
  OVALID[channel]=1;
}

static int current_in_channel=(-1); // streaming FROM analog interface on this channel
static int current_out_channel=(-1); // streaming TO analog interface on this channel

// read a value or command from serial line
fetch_val(int *chan, int *val)
{
  int status,readval;

  status=get_byte(&readval); // 1=success,0=no data, -1=error
  //if (status < 0){fprintf(stderr,"Read() failed - exiting\n");return(-1);}
  if (status <= 0) return(0); // no data (or error: but non-blocking socket returns -1 on NODATA)

// else process input
  if (DEBUG&1) printf("Rcvd %d\n",readval);
  if (readval <= 96){ // raw data on current channel
    (*chan)=current_in_channel;
    (*val)=(readval-32)&0x3f;
//    if (DEBUG&2) printf("%d ",*val);
    return(1); // show we've got a sample
  } else if (readval==126){ // attention signal from analog interface
    return(-1);
  } else if (readval > 102){ // special command?
    return(0); // and return without sample
  }

// 97-102 = channel-select command
  current_in_channel=(readval-97);
  if (DEBUG&1) printf("Changed incoming channel to %d\n",current_in_channel);
  return(0);
}

// So far, we only really use this during the initial matrix load
int process_queue(int limit) // process till empty
{
  int i,stat;

  INPUT_EVENTS=0;
  while (INPUT_EVENTS++ < limit){
    if (1==(stat=process_queue_head())) return(INPUT_EVENTS); // # events processed
  }
  return(limit);
}

// main queue processor
// pop a queue entry; see if this is an input change; if so, compute outputs,
// see which inputs they feed, and push those input changes into the queue
// return 0 on success, 1 if queue was empty

process_queue_head()
{
  int row,col,side,value,index,tick;
  int ltmp,rtmp,ktmp,lout,rout,kout,clout,crout,ckout;
  int cktmp,crtmp,cltmp;
  int bitval; // temp value for bit read from TT
  int lindex;

  if (sync_wait==1) return(2); // waiting for sync!

  if (0 != read_queue(&row,&col,&side,&value,&tick)) return(1); // queue empty

  if (tick){
//printf("Tick=%d\n",tick);
    if (tick==1){ // clock going low; bitnum will increment
      bitnum_next=bitnum+1;
      if (bitnum_next == (1<<21)) bitnum_next=0;
//printf("bitnum=%d bitnum_next=%d\n",bitnum,bitnum_next);
    }

// sweep all cells, and latch Cin and Din for each
    for (row=0;row<8;row++){
      for (col=0;col<8;col++){
        process_tick(row,col,tick);
      }
    }
    if (tick==1){
      bitnum=bitnum_next;
      if (bitnum==0) ++bitnum_iter; // count # of C-mode cycles :)
      if (0==bitnum%0x1000){
        printf("%c%d:%d     bitnum=%d:0x%06x",13,get_events(),get_time(),bitnum_iter,bitnum);
      }
    }
    return(0);
  }

// see if this is a new input value
  index=side | (col <<3) | (row << 6); // index into input[] array
  if (inputs[index] == value) return(0); // no need to process this input request
  inputs[index]=value; // else save this input value and continue...

  index=index & 0xfff8; // clear C.S1S0
  ktmp=inputs[index|K];
  rtmp=inputs[index|R];
  ltmp=inputs[index|L]; // current(new) input values to this cell (D inputs)

// find index in CM structure
  lindex=(ktmp << 3) | (rtmp << 9) | (ltmp << 15) | (col << 21) | (row << 24);
  read_CM(lindex,&kout,&rout,&lout,&ckout,&crout,&clout);

// these are the TT-specified output values for this cell...
// Find TT[bitnum] for this cell

// Read the C inputs to decide how to mix these with TT[bitnum]
  cktmp=inputs[index|CK];
  crtmp=inputs[index|CR];
  cltmp=inputs[index|CL]; // current input values to this cell (C inputs)

  lindex&=0xFFE00000; // remove L/R/K/C/S data; this is address of start of TT

// Mix value at CM[lindex+bitnum] with {r,l,k}out
  bitval=CM[lindex+bitnum]; // value from 0-63
//printf("Bitval=%d\n",bitval);
  rout=(rout*(63-crtmp))/63 + (bitval*crtmp)/63;
  lout=(lout*(63-cltmp))/63 + (bitval*cltmp)/63;
  kout=(kout*(63-cktmp))/63 + (bitval*cktmp)/63; // weighted mix of TT bit and regular Dout

// Each cout=(TT's Cout)*(63-Cin)/63 (per side)
  crout=(crout*(63-crtmp))/63;
  clout=(clout*(63-cltmp))/63;
  ckout=(ckout*(63-cktmp))/63;

// now send outputs to neighbors
// R outputs
  set_input(row,col+1,L,rout);
  set_input(row,col+1,CL,crout);

//L outputs
  set_input(row,col-1,R,lout);
  set_input(row,col-1,CR,clout);

// K outputs
  if ((row+col)&1){ // row is odd
    set_input(row-1,col,K,kout);
    set_input(row-1,col,CK,ckout);
  } else { // row is even
    set_input(row+1,col,K,kout);
    set_input(row+1,col,CK,ckout);
  }

// and ummm that's it! :D
  return(0); // done with normal processing
}

// tick routine - push the right kind of tick onto the event queue
do_tick(int level)
{
  if (0 != insqueue(TICKSIGNAL+level+1)) error(3); // Q overflow on tick
  return;
}

// This next routine will force the system to examine a cell's TT and push any output changes onto the queue
// This should be called when a cell's TT is changed.
force_output_updates(int row, int col)
{
  int side,value,index,ltmp,rtmp,ktmp,lout,rout,kout,clout,crout,ckout;
  int lindex;

  index=(col <<3) | (row << 6); // base index into input[] array
  ktmp=inputs[index|K];
  rtmp=inputs[index|R];
  ltmp=inputs[index|L]; // current input values to this cell

// find index in CM structure
  lindex=(ktmp << 3) | (rtmp << 9) | (ltmp << 15) | (col << 21) | (row << 24);
//if (lindex & 0x40000000) printf("\n\n\nWhoop!\n\n\n\n");
//printf("Reading %d %d %d %d %d\n",ktmp,rtmp,ltmp,col,row);
  read_CM(lindex,&kout,&rout,&lout,&ckout,&crout,&clout);

// these are the new output values for this cell...send them to neighbors
// use the set_input_forceable() call and specify force=1
// R output
  set_input_forceable(row,col+1,L,rout,1);
  set_input_forceable(row,col+1,CL,crout,1);

//L output
  set_input_forceable(row,col-1,R,lout,1);
  set_input_forceable(row,col-1,CR,clout,1);

// K output
  if ((row+col)&1){ // row is odd
    set_input_forceable(row-1,col,K,kout,1);
    set_input_forceable(row-1,col,CK,ckout,1);
  } else { // row is even
    set_input_forceable(row+1,col,K,kout,1);
    set_input_forceable(row+1,col,CK,ckout,1);
  }
// caller needs to let these events trickle through...
  return;
}

// set_input(): push an input change request into the event queue
// only processes an input change if the value has changed
// Note that when a cell's TT changes, its outputs may change accordingly, even if
// its inputs remain unchanged. So touching an input with set_input() won't
// have any effect
set_input(int row, int col, int side, int value)
{
  set_input_forceable(row,col,side,value,0);
}

set_input_forceable(int row, int col, int side, int value, int force) // set force=1 to force queue event
{
  int val,index,chan;
  char si;

  if (DEBUG&4) printf("Setting input [%d,%d].%c%c to %d\n",row,col,(side >3)?'C':'D', "KRL"[3&side],value);

// see if this is an edge output
  if ((row < 0) || (row >= ROWS) || (col < 0) || (col >= COLS)){ // off-edge
    for (chan=0;chan<6;chan++){
      if ((OVALID[chan]) && (OROWS[chan]==row) &&
          (OCOLS[chan]==col) && (OSIDES[chan]==side)){ // match!
// check cache!
        if (OLAST[chan] != value){
          OLAST[chan]=value; // remember this
          if (current_out_channel != chan){ // change channels
            send_byte(97+chan); // tell analog input that streaming data is for this channel
            current_out_channel=chan; // and remember we've done this
          }
          send_byte(32+(value&0x3f)); // send the data downstream
          if (DEBUG&8) printf("<chan %d=%d>\n",chan,value);
//printf("[%d,%d].%d=%d ",row,col,side,value);fflush(0);
        }
        return; // all done with this "input setting"
      }
    } // edge-output, but no assigned channel...just return
    return;
  } // end of eff-edge handling

  index=side | (col <<3) | (row << 6); // index into input[] array
  if (inputs[index] == value) return(0); // no need to process this input request
  ++INPUT_EVENTS; // count actual input events processed

  if ((row < 0) || (row > 7) || (col < 0) || (col > 7)) return; // shouldn't get here...

  val=(value&0x3f) | (side<<6) | (col<<9) | (row << 12); // 14-bit entry
  if (0 != insqueue(val)) error(2); // Q overflow
  return;
}

// push something into the tail of the queue
insqueue(int value)
{
  ++events; // count total # of events
  if (timeflag==1) value|=TIMESIGNAL; // set flag

  queue[qtail]=value;++qtail;
  if (qtail == QSIZE) qtail=0;
  if (qtail==qhead) return(1); // overflow :(
  return(0);
}

// read head of queue, and breakout into fields
read_queue(int *row,int *col,int *side,int *value,int *tick)
{
  int i;
  i=remqueue();
  if (i == -1) return(1); // empty queue
  if (i&TICKSIGNAL){ // clock tick
    *tick = (i-TICKSIGNAL); // 1=low, 2=high
//printf("TICK!!! %d\n\n",(*tick));
    return(0);
  }

// otherwise, this is an input-change event
  *tick=0;
  *value=(i&0x3f);
  *side=(i>>6)&0x7;
  *col=(i>>9)&0x7;
  *row=(i>>12)&0x7;
  return(0);
}

// raw queue removal code
remqueue()
{
  int i;

  if (qhead==qtail) return(-1); // empty
  i=queue[qhead];++qhead;
  if (qhead==QSIZE) qhead=0;
// has time passed?
  if (((i&TIMESIGNAL) && (timeflag)) ||
      (((i&TIMESIGNAL)==0) && (timeflag==0))){ // we're processing an event pushed at the current timestep; so increment time
    ++realtime;
    timeflag=(1-timeflag);
  }
  i=i&0x17fff; // clear timesignal flag
  return(i);
}

// Do whatever initialization is necessary for the main CM TT array
init_CM(int rows, int cols)
{
  int i,size;

  size=(2*1024*1024) *cols*rows; // 2 MB per cell
  CM=(char *)malloc(size);
  if (CM == 0) {error(1);return(1);} // error

  ROWS=rows;COLS=cols; // save for future reference
  for (i=0;i<size;i++) CM[i]='\0'; // empty truth tables!

  for (i=0;i<512;i++) inputs[i]=0;
  return(0); // success!
}

// read main CM array for given [r,c]L,R,K inputs; return k,r,l outputs
read_CM(int lindex,int *kout,int *rout,int *lout,int *ckout, int *crout, int *clout)
{
  *kout=CM[lindex | K];
  *rout=CM[lindex | R];
  *lout=CM[lindex | L];
  *ckout=CM[lindex | CK];
  *crout=CM[lindex | CR];
  *clout=CM[lindex | CL];
}

// pass back the address of the CM[] array (mainly for file dump)
char *fetch_CM()
{
  return(CM);
}

int error(int i)
{
  switch (i){
    case 1: printf("CM malloc failed\n");break;
    case 2: printf("Q overflow on insert\n");break;
    case 3: printf("Q overflow on tick\n");break;
    default: printf("Unknown error %d\n",i);break;
  }
  return(i);
}

// raw TT set
set_tt(int row, int col, int l, int r, int k, int side, int value)
{
  int lindex;

  side=side&7;col=col&7;row=row&7;l=l&63;r=r&63;k=k&63;value=value&63; // SKIP?
  lindex=side | (k << 3) | (r << 9) | (l <<15) | (col << 21) | (row << 24);
  CM[lindex]=value;
}

int sync_count=0; // # of writes since last sync

// lowest-level communication code: could be serial, ethernet, wifi, etc.
send_byte(int c)
{
  char buffer[2];

  if ((sync_wait==1) && (sync_count > 109)) printf("Ooops! Sending lots of stuff during sync wait - check this!\n");

  if (DEBUG & 0x10) printf("Raw send: %d\n",c&0x7f);
  buffer[0]=(char) (c & 0x7f);
  //if (1 != write(fp,buffer,1)) printf("Ooops");
  write(fp,buffer,1);
  if (++sync_count==100){
    send_sync();
    sync_count=0;
  }
}

int get_byte(int *value)
{
  char buffer[2];
  int status;

  status=read(fp,buffer,1);
  if (status != 1) return(status);

// Is this a SYNC response?
  if (buffer[0]==123){sync_wait=0;return(0);}

// return value
  (*value)=buffer[0]&0x7f;
  return(1);
}

int nansleep(long t)
{
  struct timespec t1,t2;
  int i,x=0;
  for (i=0;i<10000;i++) x=x+i;
  return(x);
  t1.tv_sec=0;
  t1.tv_nsec=t; // # usec
  nanosleep(&t1,&t2);
}

// Access simulated IO on port 1221.
// Server (simulated box) must already be running
socket_init()
{
  int sock,status;
  struct hostent *server;
  struct sockaddr_in serv_addr;

// make a non-blocking socket
  sock=socket(AF_INET,SOCK_STREAM/*|SOCK_NONBLOCK*/,0);
  if (sock < 0){
    fprintf(stderr,"ERROR: Can't open socket\n");
    return(1);
  }

// get address infor for our connection, and build the structure
  server=gethostbyname("localhost");
  if (server == NULL){
    fprintf(stderr,"ERROR: Can't resolve \"localhost\" (that's weird!)\n");
    return(1);
  }
  bzero((char *) &serv_addr,sizeof(serv_addr)); // zero-out the structure
  serv_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr,(char *)&serv_addr.sin_addr.s_addr,server->h_length);
  serv_addr.sin_port = htons(1221);
  if (connect(sock,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){
    fprintf(stderr,"ERROR: Can't connect to server (port 1221)\n");
    return(1);
  }
  fcntl(sock, F_SETFL, O_NONBLOCK); // make this non-blocking now
  fp=sock;
// should be connected now - few to read() and write() on fp
  printf("Connected to simulated IO unit\n");
  return(0);
}

serial_init(char *name)
{
  fp=open(name,O_RDWR|O_NONBLOCK);
  if (fp < 0){printf("error opening %s\n",name);return(1);}
  return(0); // success
}

// set an edge input to the array
set_edge(int chan,int value)
{
  if (IVALID[chan]) set_input(IROWS[chan],ICOLS[chan],ISIDES[chan],value);
}

// Turn off all channel monitoring
clear_channel_monitoring()
{
  send_byte(124);send_byte(32); // clear all channel monitoring
}

// ask analog interface to re-send all monitored analog values
request_aval_resend()
{
  send_byte(125); // request interface to re-send all analog values
}

// we can send things much faster the the Analog IO device can process them
// so send a SYNC pulse periodically, and idle until it's returned to us
int send_sync()
{
  send_byte(123);
  sync_wait=1; // don't process queue events until this clears
}

int sync_waiting()
{
  return(sync_wait);
}

// process a single tick for one cell
process_tick(int row, int col, int tick) // tick=1:low or 2:high
{
  int index,lindex,side,ktmp,rtmp,ltmp,cktmp,crtmp,cltmp;
  int cla,dla,bitval;
  int kout,rout,lout,ckout,crout,clout;

  if (tick==2){ // clock->1
    index=(col <<3) | (row << 6); // index into input[] array
    ktmp=inputs[index|K];
    rtmp=inputs[index|R];
    ltmp=inputs[index|L];
    cktmp=inputs[index|CK];
    crtmp=inputs[index|CR];
    cltmp=inputs[index|CL]; // C and D inputs. Compute C- and D-Latch values

// latch max of all Cin
    if (cktmp > crtmp){
      if (cktmp > cltmp) cla=cktmp; else cla=cltmp;
    } else {
      if (crtmp > cltmp) cla=crtmp; else cla=cltmp;
    }
    if (cla==0){
      clatch[row][col]=0;
      dlatch[row][col]=0;
//if (row+col==0000) printf("Clock High: latched 0\n");
      return;
    }
// otherwise, latch a mix of the D inputs, weighted by the corresponding Cins
    clatch[row][col]=cla;
    dlatch[row][col]=(ktmp*cktmp + rtmp*crtmp + ltmp*cltmp)/(cktmp+crtmp+cltmp);
//if (row+col==0000) printf("Clock High: latched D=%d C=%d\n",dlatch[row][col],clatch[row][col]);
    return;
  } // end of clock=1

// for clock->0, load a combo of latched bit and TT's current bit -> TT[bitnum]

// find index in CM structure
  index=(col << 21) | (row << 24);
  bitval=CM[index+bitnum]; // current value, from 0-63
  CM[index+bitnum]=(bitval*(63-clatch[row][col]))/63 +
                   (dlatch[row][col]*clatch[row][col])/63;
//if (row+col==0000) printf("TT change: bitnum %d (%d->%d), [%d,%d]\n",bitnum,bitval,CM[index+bitnum],row,col);
//if (CM[index+bitnum] != bitval) printf("TT change: bitnum %d (%d->%d), [%d,%d]\n",bitnum,bitval,CM[index+bitnum],row,col);

// Now set outputs based on new TT and current inputs
// (same as in processing an input change)
  index=(col <<3) | (row << 6); // index into input[] array
  ktmp=inputs[index|K];
  rtmp=inputs[index|R];
  ltmp=inputs[index|L]; // current(new) input values to this cell (D inputs)

// find index in CM structure
  lindex=(ktmp << 3) | (rtmp << 9) | (ltmp << 15) | (col << 21) | (row << 24);
  read_CM(lindex,&kout,&rout,&lout,&ckout,&crout,&clout);

// these are the TT-specified output values for this cell...
// Find TT[bitnum] for this cell

// Read the C inputs to decide how to mix these with TT[bitnum]
  cktmp=inputs[index|CK];
  crtmp=inputs[index|CR];
  cltmp=inputs[index|CL]; // current input values to this cell (C inputs)

  lindex&=0xFFE00000; // remove L/R/K/C/S data; this is address of start of TT

// Mix value at CM[lindex+bitnum] with {r,l,k}out
  bitval=CM[lindex+bitnum_next]; // value from 0-63
// note we want to use the next bitnum value, which won't appear till we return
//if (row+col==0) printf("TT[%d (not %d)]=%d\n",bitnum_next,bitnum,bitval);
  rout=(rout*(63-crtmp))/63 + (bitval*crtmp)/63;
  lout=(lout*(63-cltmp))/63 + (bitval*cltmp)/63;
  kout=(kout*(63-cktmp))/63 + (bitval*cktmp)/63; // weighted mix of TT bit and regular Dout

// Each cout=(TT's Cout)*(63-Cin)/63 (per side)
  crout=(crout*(63-crtmp))/63;
  clout=(clout*(63-cltmp))/63;
  ckout=(ckout*(63-cktmp))/63;

// now send outputs to neighbors
// R outputs
  set_input(row,col+1,L,rout);
  set_input(row,col+1,CL,crout);

//L outputs
  set_input(row,col-1,R,lout);
  set_input(row,col-1,CR,clout);

// K outputs
  if ((row+col)&1){ // row is odd
    set_input(row-1,col,K,kout);
    set_input(row-1,col,CK,ckout);
  } else { // row is even
    set_input(row+1,col,K,kout);
    set_input(row+1,col,CK,ckout);
  }

// end of clock->0 processing
  return;
}

void reset_input_events()
{
  INPUT_EVENTS=0;
}

int get_input_events()
{
  return(INPUT_EVENTS);
}
