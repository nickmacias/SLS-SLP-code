// 3-sided cells, alternating-triangle arrangement
#define K 0 // top/bottom
#define R 1 // right
#define L 2 // left

// C lines...
#define CK 4
#define CR 5
#define CL 6

#define TIMESIGNAL 0x8000 // for counting event ticks

#define TICKSIGNAL 0x10000 // tick indicator in queue :)
// 0x10001=Low, 0x10002=High

// defines for setting TT bits
#define SET_TT(ROWNUM,COLNUM,SIDE,EQN) set_tt(ROWNUM,COLNUM,lval,rval,kval,SIDE,EQN)

#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <time.h>

int setup_input_channel(int row,int col,int side,int channel);
int setup_output_channel(int row,int col,int side,int channel);
int fetch_val(int *chan, int *val);
int process_queue(int limit); // process till empty, up to "limit" events. Returns # events processed
int process_queue_head();
int set_input(int row, int col, int side, int value);
int insqueue(int value);
int read_queue(int *row,int *col,int *side,int *value,int *tick);
int remqueue();
int init_CM(int rows,int cols);
int read_CM(int lindex,int *kout,int *rout,int *lout, int *ckout, int *crout, int *clout);
int error(int i);
int set_tt(int row, int col, int l, int r, int k, int side, int value);
int send_byte(int c);
int get_byte(int *value);
int nansleep(long t);
int serial_init(char *name);
int set_edge(int chan,int value);
char *fetch_CM();
int clear_channel_monitoring();
int request_aval_resend();
int force_output_updates(int row, int col);
int send_sync();
int sync_waiting();
int do_tick(int level);
void reset_input_events();
int get_input_events();
