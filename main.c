#include <stdio.h>
#include <fcntl.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include "engine.h"

#define LIMIT 50    // # input events allowed per queue process

/*
 * Main CM3G Simulation Engine
 * Communicates with either a real hardware interface (vis /dev/ttyUSB#)
 * or a simulated interface (via a socket).
 *
 * Accepts 3 argumenst:
 *  IO channel: /dev/ttyUSB# or "-" for port 1221
 *  TT file - which should be compiled with the "comp" (synth_tt) command
 *  cfg file - which specifies input- and output-channel assignments
 *
 */

main(int argc,char **argv)
{
  int fetch_status,chan,value,stat,i;
  int updatecount=0;

// See if we have a serial port...
  if (argc != 4){
    fprintf(stderr,"Usage: %s [serialdevice or -] TT_File Config_File\n",argv[0]);
    return(1);
  }

  if (init_IO(argv[1])) return(1); // setup serial- or socket-based IO

// setup channels first, since we'll want to potentially be setting outputs to something interesting
// as a result of loading initial truth tables into the cells

  clear_channel_monitoring();
  if (ingest_cfg(argv[3])) return(1); // read channel configuration info
  request_aval_resend();

  if (ingest_tt(argv[2])) return(1); // read (compiled) TT file and load into CM[]

  for (i=0;i<100;i++) fetch_val(&chan,&value); // flush stray signals

// main loop: read from serial port and set edge input appropriately, then process queue
// (queue processing will send outputs from edge changes)

  reset_input_events(); // set to 0
  while (1==1){
//printf("UC=%d\n",updatecount);
    if (0==(updatecount=(++updatecount)%5000)){
      printf("%c%d:%d",13,get_events(),get_time());
    }
    fetch_status=fetch_val(&chan,&value);
    if (fetch_status==(-1)) break; // leave this loop
    if (fetch_status){ // we read a new value
       set_edge(chan,value); // assert this edge input
    }
    stat=process_queue_head(); // interleave with input changes
//printf("process_queue_head()=%d\n",stat);
    if ((stat==0) && (get_input_events() > 5000)){ // 0 means processed an event
      printf("\nQueue instability?\n");
      stat=1; // force clock tick
    }

// If queue is empty (stat=1) (or unstable), push a clock tick...
    if (stat==1){
//printf("TICK!!!\n\n");
      tick();
      reset_input_events();
    }
    //nansleep(1); // 1 mSec
  }
// exited due to abnormal condition
  printf("Attention signal detected - goodbye.\n");
}

// Call this to generate another tick (high or low)
static int ticklevel=0;

tick()
{
  do_tick(ticklevel); // insert this tick level into the queue
  ticklevel=1-ticklevel; // and toggle for next tick
}

int grab_line(int fp, char *buffer)
{
  int i;
  for (i=0;i<115;i++){
    read(fp,&buffer[i],1);
    if (buffer[i]=='\n') break;
  }
  if (i > 113){ // didn't find newline
    buffer[0]='\0';
  } else buffer[i]='\0'; // replace \n with null
}

int init_IO(char *name)
{
// open serial device
  if (0==strcmp(name,"-")){ // IO via socket
    if (socket_init()) return(1); // failed
  } else { // IO via serial port
    if (serial_init(name)) return(1); // error
  }
  return(0);
}

int ingest_tt(char *name)
{
  int i,row,col,tt_fp,ROWS,COLS,index,stat;
  char *MyCM,buffer[120];

// open TT file, get header info
  tt_fp=open(name,O_RDONLY);
  if (tt_fp < (long int) NULL){
    fprintf(stderr,"Can't open %s\n",name);
    return(1);
  }

  grab_line(tt_fp,buffer);
  if (0 != strcmp(buffer,"V3G.noc.1.0")){
    fprintf(stderr,"%s doesn't have the expected header \"V3G.noc.1.0\"\n",name);
    close(tt_fp);
    return(1);
  }

// read dims, rows and cols
  grab_line(tt_fp,buffer);
  if (1 != sscanf(buffer,"%d",&i)){
    fprintf(stderr,"ERROR: Undefined # of dimensions: <%s>\n",buffer);
    close(tt_fp);
    return(1);
  }
  if (i != 2){
    fprintf(stderr,"Expecting a two-dimensional matrix\n");
    close(tt_fp);
    return(1);
  }

  grab_line(tt_fp,buffer);
  if (1 != sscanf(buffer,"%d",&ROWS)){
    fprintf(stderr,"ERROR: Undefined dimension: <%s>\n",buffer);
    close(tt_fp);
    return(1);
  }
  grab_line(tt_fp,buffer);
  if (1 != sscanf(buffer,"%d",&COLS)){
    fprintf(stderr,"ERROR: Undefined dimension: <%s>\n",buffer);
    close(tt_fp);
    return(1);
  }

  printf("Array is %dx%d\n",ROWS,COLS);

  if (init_CM(ROWS,COLS)){ // failure
    close(tt_fp);
    return(1);
  }

  printf("CM Engine online and ready\n");

  MyCM=fetch_CM(); // get a pointer to the CM structure, so we can load it up from the file

// load TTs from input file, while forcing output changes and queue processing...
  for (row=0;row<ROWS;row++){
    for (col=0;col<COLS;col++){
      printf("%c                   %cReading [%d,%d]",13,13,row,col);fflush(stdout);
      //index=(64*64*64)*4*(row*COLS + col);
      index=(col << 21) | (row << 24);
      read(tt_fp,&MyCM[index],64*64*64*8);
// that loads this cell's TT...next we need to let output changes happen
      force_output_updates(row,col);
// now process these through the queue engine
      reset_input_events();
      while (0 == (stat=process_queue_head())){
        if ((stat==0) && (get_input_events() > 1000)){ // 0 means processed an event
          printf("Queue instability on matrix load?\n");
          break;
        }
      }
    }
  }
  reset_input_events();
  printf("\n");

// should be at end of file
  grab_line(tt_fp,buffer);
  if (0 != strcmp(buffer,"EOF")){
    fprintf(stderr,"Didn't find EOF\n");
    close(tt_fp);
    return(1);
  }

  close(tt_fp);
//for (i=0;i<16;i++) printf("CM[%d]=%d\n",i,MyCM[i]);

  return(0);
}

// IO configuration
int ingest_cfg(char *name)
{
  FILE *fp;
  char buffer[120],buf2[120];;
  int i,j,row,col,chan,side_num;
  char side,cd;

  fp=fopen(name,"r");
  if (fp <= (FILE*) NULL){
    fprintf(stderr,"Can't open %s\n",name);
    return(1);
  }

// read each line, and call setup_input_channel() or setup_output_channel()
  printf("\n---------------------------\n");
  while (NULL != fgets(buffer,120,fp)){
    if (buffer[-1+strlen(buffer)] == '\n') buffer[-1+strlen(buffer)]='\0'; // trim NL
    for (i=j=0;i<strlen(buffer);i++){
      if ((buffer[i] != ' ') && (buffer[i] != '\t')) buf2[j++]=toupper(buffer[i]);
    }
    buf2[j]='\0';

// buf2 is cleaned-up

    if (buf2[0]=='\0') continue;
    if (buf2[0]=='#') continue;

    if (5 == sscanf(buf2,"[%d,%d]%c%c->%d",&row,&col,&cd,&side,&chan)){
      switch(side){
        case 'L': side_num=L;break;
        case 'R':side_num=R;break;
        case 'K':side_num=K;break;
        default:fprintf(stderr,"Illegal side \"%c\" in cfg string <%s>\n",side,buffer);return(1);
      }

      if (cd=='C')
        side_num+=4;
      else if (cd != 'D'){
        fprintf(stderr,"Expecting side to begin with C or D but started with %c\n",cd);
        return(1);
      }

      setup_output_channel(row,col,side_num,chan);
      printf("Monitoring [%d,%d]%c%c on Output Channel %d\n",row,col,cd,side,chan);
    } else if (5 == sscanf(buf2,"%d->[%d,%d]%c%c",&chan,&row,&col,&cd,&side)){
      switch(side){
        case 'L': side_num=L;break;
        case 'R':side_num=R;break;
        case 'K':side_num=K;break;
        default:fprintf(stderr,"Illegal side \"%c\" in cfg string <%s>\n",side,buffer);return(1);
      }

      if (cd=='C')
        side_num+=4;
      else if (cd != 'D'){
        fprintf(stderr,"Expecting side to begin with C or D but started with %c\n",cd);
        return(1);
      }

      setup_input_channel(row,col,side_num,chan);
      printf("Input Channel %d is driving [%d,%d]%c%c\n",chan,row,col,cd,side);
    } else {
      fprintf(stderr,"Can't decipher cfg line: <%s>\n",buffer);
      return(1);
    }
  }
  printf("---------------------------\n\n");

  fclose(fp);
  return(0); // Success!
}
