// pull these from a #include file

#define K 0
#define R 1
#define L 2

void setup()
{
  Serial.begin(9600);
  pinMode(2,OUTPUT);    // MSB
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);    // LSB
  pinMode(8,INPUT);     // Attention!
  pinMode(9,OUTPUT);    // WRITEBAR
  digitalWrite(9,HIGH);
  pinMode(10,OUTPUT);   // CHAN02BAR/CHAN13
  pinMode(11,OUTPUT);   // CS01BAR
  pinMode(12,OUTPUT);   // CS23BAR
  analogReference(DEFAULT); // 5V
//  analogReference(INTERNAL);
}

static int chan=2;
static int val=32;

void loop()
{
  int i;
 
/***  
  set_dout(val,0);
  if (++val > 95) val=32;
  Serial.println(val);
  delay(1000);
  return;
***/

  while (Serial.available() == 0);
  i=Serial.read(); // 32-96
  Serial.write(i);
  
  if (i==32){
    if (chan==2) chan=3; else chan=2;
    Serial.print("Switched to channel ");Serial.println(chan);
  }
  if ((i > 32) && (i < 96)) set_dout(i,chan);
}

// send an analog value to the main engine
void send_aval(int value,int channel)
{
  value=(value>>4)&0x3f; // from full range (10 bits) to 6 bits (0-63)
  Serial.write(32+value);
}


void set_dout(int value,int channel) // drive the D-to-A pins
{
  int i,pin,mon_chan;
  int dout[8]; // save each bit first
  
  value=value-32; // return to true 6-bit value
// use an array so we can slam the bits out more-quickly (or use direct register I/O?)
  pin=2; // start with LSB
//value=32;
  for (i=0;i<6;i++){
    dout[pin++]=(value&1)?HIGH:LOW;
    value=value>>1;
  }
// now output these really quickly
  digitalWrite(2,dout[2]);
  digitalWrite(3,dout[3]);
  digitalWrite(4,dout[4]);
  digitalWrite(5,dout[5]);
  digitalWrite(6,dout[6]);
  digitalWrite(7,dout[7]);
// select which DAC
//channel=0;
  digitalWrite(10,(channel&1)?HIGH:LOW); // select CHAN0BAR/CHAN1
  if (0==(channel&2)){digitalWrite(11,HIGH);digitalWrite(12,LOW);} // select first DAC
    else {digitalWrite(11,LOW);digitalWrite(12,HIGH);} // select second DAC
  delayMicroseconds(1);
  digitalWrite(9,LOW);delayMicroseconds(1);digitalWrite(9,HIGH); // pulse the WRBAR line
}
