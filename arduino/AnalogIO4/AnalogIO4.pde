// pull these from a #include file

#define K 0
#define R 1
#define L 2

void setup()
{
  Serial.begin(1000000);
  //Serial.begin(9600);
  pinMode(2,OUTPUT); // MSB
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT); // LSB
  pinMode(8,INPUT); // Attention!
  
  pinMode(9,OUTPUT);   // WRITE BAR
  digitalWrite(9,HIGH); // normally off
  
  pinMode(10,OUTPUT);  // CHAN0 BAR/CHAN1
  pinMode(11,OUTPUT);  // CS01BAR/CS23
  
// To update analog out:
//   o send the digital data to pins 2-7
//   o select channel on 11:10 (LSB) e.g. 00=chan 0, 01=chan 1, 10=chan 2, 11=chan 3
//   o drop the WRITE BAR line (pin 9) and then return it to high

  analogReference(DEFAULT); // 5V
//  analogReference(INTERNAL);
}

int lastval[6]={-1,-1,-1,-1,-1,-1}; // don't re-send same value
// initializing to -1 will force an initial send, since analog value will definitely differ!
int last_in_channel=(-1);  // channel number for last value we sent

int last_out_channel=(-1); // channel number for last data received from CM engine

int monitor=0; // each bit shows whether or not we're monitoring that input

void loop()
{
  int aval, readback;

  if (digitalRead(8) == HIGH){ // Attention - tell CM engine to exit (or break?)
    Serial.write(126);
  }
  
// Read analog inputs, condition, and send to CM
  if (monitor&0x01) {aval=analogRead(0);send_aval(aval,0);} // send to channel 0
  if (monitor&0x02) {aval=analogRead(1);send_aval(aval,1);}
  if (monitor&0x04) {aval=analogRead(2);send_aval(aval,2);}
  if (monitor&0x08) {aval=analogRead(3);send_aval(aval,3);}
  if (monitor&0x10) {aval=analogRead(4);send_aval(aval,5);}
  if (monitor&0x20) {aval=analogRead(5);send_aval(aval,5);}

// see if serial data is coming to us
  if (0 != Serial.available()){
    readback=Serial.read(); // word from CM engine
    
    if (readback==125){ // request to re-send all analog values
      lastval[0]=lastval[1]=lastval[2]=lastval[3]=lastval[4]=lastval[5]=(-1);
    } else if (readback==124) { // channel monitor command - read next word
      while (0==Serial.available());
      readback=Serial.read();
      if (readback==32) monitor=0; // clear all monitors
      else if ((readback > 32) && (readback < 39)){ // monitor a channel
        monitor|=(1<<(readback-33));
        lastval[readback-33]=(-1); // force a send of this channel's value to engine
      }
    } else if ((readback>=97)&&(readback<=102)){ // streaming data FROM CM engine is on this channel
      last_out_channel=readback-97;
    } else { // not a special command: send to current output channel
      set_dout(readback,last_out_channel);
    }
  }
}

// send an analog value to the main engine
void send_aval(int value,int channel)
{
  value=(value>>4)&0x3f; // from full range (10 bits) to 6 bits (0-63)
  if (value != lastval[channel]){ // need to send this value
    if (channel != last_in_channel){ // specify a new channel first
      Serial.write(97+channel); // special challen-select command
      last_in_channel=channel;
    }
    Serial.write(32+value);
    lastval[channel]=value;
  }
}

// Eventually, we'll output to one of 4 (or 8) D2A converters
// For now, we only have one D2A; so output if the external monitor-channel input matches the requested set_dout channel

void set_dout(int value,int channel) // drive the D-to-A pins
{
  int i,pin,mon_chan;
  int dout[8]; // save each bit first
  
  //mon_chan=((digitalRead(9)==HIGH)?1:0) + ((digitalRead(10)==HIGH)?2:0);
  //if (mon_chan != channel) return;

  value=value-32; // return to true 6-bit value
// use an array so we can slam the bits out more-quickly (or use direct register I/O?)
  pin=2; // start with LSB

  for (i=0;i<6;i++){
    dout[pin++]=(value&1)?HIGH:LOW;
    value=value>>1;
  }
// now output these really quickly
  digitalWrite(7,dout[7]);
  digitalWrite(2,dout[2]);
  digitalWrite(6,dout[6]);
  digitalWrite(3,dout[3]);
  digitalWrite(5,dout[5]);
  digitalWrite(4,dout[4]); // crazy order not necessary now, since output won't change till we drop WRITEBAR
  
  digitalWrite(10,(channel&1)?LOW:LOW);
  digitalWrite(11,(channel&2)?LOW:LOW); // this selects the proper DAC
  digitalWrite(9,LOW);delay(100);digitalWrite(9,HIGH);
}
