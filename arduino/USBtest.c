#include <stdio.h>
#include <fcntl.h>

main()
{
  int fp,i;
  char buffer[16];
  char LED=0;

  fp=open("/dev/ttyUSB0",O_RDWR|O_NONBLOCK);
  if (fp < 0){
    fprintf(stderr,"Can't open serial device");
    return(1);
  }

// read conseuctive ints. Every 10,000 exchanges, send A/B
  while (1==1){
    for (i=0;i<100000;i++){
      int status=read(fp,buffer,1);
      if (status != 1) continue;
      if (i < 10) printf("%d ",(int) buffer[0]);
      if (i==9) printf("\n");
      buffer[0]='x';
      write(fp,buffer,1);
    }
// that's 10,000 exchanges...now toggle the LED
    read(fp,buffer,1);
    LED=~LED;
    buffer[0]=(LED?'A':'B');
    write(fp,buffer,1);
  } // loop forever
}
