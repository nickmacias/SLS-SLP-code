// pull these from a #include file

#define K 0
#define R 1
#define L 2

void setup()
{
  Serial.begin(1000000);
  //Serial.begin(9600);
  pinMode(2,OUTPUT); // MSB
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT); // LSB
  analogReference(DEFAULT); // 5V
//  analogReference(INTERNAL);
}

int lastval=9999;

void loop()
{
  int aval, readback;

// Read analog input, condition, and send to CM
  aval=analogRead(3); // 0-1023
  aval=aval>>4; // 6 bits
  if (lastval != aval)  send_byte(aval,0,0,L);
  lastval=aval;
//  delay(2); // 500 hz max
  
// see if serial data is coming to us
  if (0 != Serial.available()){
    readback=Serial.read(); // package this so we can read a row, col and side as well as the value
    readback=readback&0x7f; // just in case?
    set_dout(readback);
  }
}

void send_byte(int val, int row, int col, int side)
{
// see what [r,c]side we're currently streaming to, and adjust if necessary
// but for now, always assume [0,0]L
  Serial.write((val&0x3f) + 32); // 6-bits => Ascii code from 32-95 (no funny characters!)
}

void set_dout(int value) // drive the D-to-A pins
{
  int i,pin;

  value=value-32; // return to true 6-bit value
// use an array so we can slam the bits out more-quickly (or use direct register I/O?)
  pin=7; // start with LSB
  for (i=0;i<6;i++){
    digitalWrite(pin,(value&1)?HIGH:LOW);
    --pin;
    value=value>>1;
  }
}
