import termios
import sys

print "Pre-conditioning",sys.argv[1]
speed = termios.B9600
serial = file(sys.argv[1], 'r+', 0)
ts = termios.tcgetattr(serial)
ts[0] &= ~(termios.IGNBRK | termios.BRKINT | termios.PARMRK | termios.ISTRIP | termios.INLCR | termios.IGNCR | termios.ICRNL | termios.IXON | termios.IXOFF)
ts[1] &= ~termios.OPOST
ts[2] &= ~(termios.CSIZE | termios.PARENB | termios.HUPCL)
ts[2] |= termios.CS8
ts[3] &= ~(termios.ECHO | termios.ECHONL | termios.ICANON | termios.ISIG | termios.IEXTEN)
ts[4] = speed
ts[5] = speed
ts[6][termios.VMIN] = 0
ts[6][termios.VTIME] = 0
termios.tcsetattr(serial, termios.TCSANOW, ts)
