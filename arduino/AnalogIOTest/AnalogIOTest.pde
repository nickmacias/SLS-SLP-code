// pull these from a #include file

#define K 0
#define R 1
#define L 2

void setup()
{
  Serial.begin(9600);
  pinMode(2,OUTPUT); // MSB
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT); // LSB
  pinMode(8,INPUT); // Attention!
  
  pinMode(9,OUTPUT);   // WRITE BAR
  digitalWrite(9,HIGH); // normally off
  
  pinMode(10,OUTPUT);  // CHAN0 BAR/CHAN1
  pinMode(11,OUTPUT);  // CS01BAR/CS23
  digitalWrite(10,LOW);
  digitalWrite(11,LOW);  
  
// To update analog out:
//   o send the digital data to pins 2-7
//   o select channel on 11:10 (LSB) e.g. 00=chan 0, 01=chan 1, 10=chan 2, 11=chan 3
//   o drop the WRITE BAR line (pin 9) and then return it to high

  analogReference(DEFAULT); // 5V
//  analogReference(INTERNAL);
}

void loop()
{
  int aval, readback;

  aval=analogRead(1);
//  Serial.print(aval);
  //Serial.write(32+(aval>>4));
  set_dout(32+(aval>>4),1);
  delay(50);
}

int lastval=(-1);

void set_dout(int value,int channel) // drive the D-to-A pins
{
  int i,pin,mon_chan;
  int dout[8]; // save each bit first

  if (value==lastval) return;
  lastval=value; // remember
  
  //mon_chan=((digitalRead(9)==HIGH)?1:0) + ((digitalRead(10)==HIGH)?2:0);
  //if (mon_chan != channel) return;

  value=value-32; // return to true 6-bit value
// use an array so we can slam the bits out more-quickly (or use direct register I/O?)
  pin=2; // start with LSB
Serial.print(value);Serial.print("\n");
  for (i=0;i<6;i++){
    dout[pin++]=(value&1)?HIGH:LOW;
    value=value>>1;
  }
// now output these really quickly
  digitalWrite(7,dout[7]);
  digitalWrite(2,dout[2]);
  digitalWrite(6,dout[6]);
  digitalWrite(3,dout[3]);
  digitalWrite(5,dout[5]);
  digitalWrite(4,dout[4]); // crazy order not necessary now, since output won't change till we drop WRITEBAR
  
//  digitalWrite(10,(channel&1)?LOW:LOW);
//  digitalWrite(11,(channel&2)?LOW:LOW); // this selects the proper DAC
  digitalWrite(9,LOW);delay(100);digitalWrite(9,HIGH);
}
