#include <stdio.h>
#include  "WProgram.h"
#include  "HardwareSerial.h"
#include "LCD_driver.h"
#include "nokia_tester.h"


long nextmilli;
int hours,mins,secs;

void setup()
{
  ioinit(); //Initialize ARM I/O
  analogReference(DEFAULT);
  LCDInit();      //Initialize the LCD
  LCDClear(BLACK);
  Serial.begin(9600);
}

int time=127; // sweeps to 128
int saveval[128]; // so we can erase old pixels
int lastval=0; // for triggering
int trigger;

void loop()
{
  int sweepspeed,amp,signal;  // analog inputs
  int x,y; // for positioning cursor

  if (time >= 0){
    if (++time>127){
      time=(-1); // waiting to drop below trigger threshold
      Serial.println("time==-1");
    }
  }
  trigger=analogRead(1)/8;
  sweepspeed=analogRead(3);
  //amp=analogRead(1)/8;
  amp=8;
  signal=analogRead(2)/4; // input signal
//  signal=signal*amp;

// Clear current point
  if (time >= 0) LCDSetPixel(BLACK,(time+2)&0x7f,saveval[(time+2)&0x7f]);
  
// delay a bit based on externally-supplied sweepspeed
  sweepspeed=(1023-sweepspeed); // max total sweep==1 ms*128 timesteps=128 ms
  sweepspeed*=10; // 1 sec sweep
  delayMicroseconds(sweepspeed);

  if (time==-1){
    if (signal < trigger){
      time=(-2); // waiting to rise above trigger
    }
  }
  
  if (time==-2){
    if (signal > trigger+16){
      time=0; // start trace
    }
  }
  
// Draw pixel now
  y=127-signal;

  
  if (time >= 0){
    if (y < 128) LCDSetPixel(WHITE,time,y);
  }
  lastval=y; // for future triggering

// and save this
  if (time >= 0) saveval[time]=y;
}

